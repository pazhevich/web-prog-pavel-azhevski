function SearchGoogle() {
    const request = document.getElementsByClassName("requestToSearch")[0].value;
    if (request) {
        document.location.href = ('https://www.google.by/search?q=' + request);
        document.getElementsByClassName("requestToSearch")[0].value = null;
    }
}
function ShowTopNav() {
    const div = document.getElementsByClassName("mob_drop_menu")[0];
    var computetStyle = getComputedStyle(div);
    if (computetStyle.display == "none") div.style.display = "block";
    else div.style.display = "none";
}