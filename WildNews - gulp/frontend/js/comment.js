function AddComment() {
    const comment = document.getElementsByClassName("com_input")[0].value;
    if (comment) {
        var divcomment = document.createElement("div");
        divcomment.innerHTML = "<div class='comment'><div class='login'>You:</div>" + comment + "</div>";
        document.getElementById("com_container").appendChild(divcomment);
        document.getElementsByClassName("com_input")[0].value = null;
        return false;
    }
}