$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100 && window.innerWidth > 660) {
            $('.to_top_button').fadeIn();
        } else {
            $('.to_top_button').fadeOut();
        }
    });

    $('.to_top_button').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 600);        
        return false;
    });

});