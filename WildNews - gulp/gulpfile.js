'use strict';

const  gulp = require('gulp');
const less = require('gulp-less');
const path = require('path');
const del=require('del');

gulp.task('less', function () {
    return gulp.src('frontend/styles/**/*.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('clean',function () {
    return del('public');
});

gulp.task('html',function () {
    return gulp.src('frontend/assets/html/**/*.html')
        .pipe(gulp.dest('public'));
});

gulp.task('img',function(){
   return gulp.src('frontend/assets/images/**')
       .pipe(gulp.dest('public/images'))
});

gulp.task('js',function () {
    return gulp.src('frontend/js/**')
        .pipe(gulp.dest('public/js'))
});



gulp.task('build',gulp.series('clean',gulp.parallel('less','html','img','js')));